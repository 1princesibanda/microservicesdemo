#!/bin/sh
echo 1..1
name="anyName"
output=`mpirun -np 3 src/sendName -n $name`
expected="Hello $name, i am your father!
Hello $name, i am your father!"
if [ $? -eq 0 ]; then
	if [ "$output" == "$expected" ]; then
		echo 'ok 1 - test sending with three process system correct'
	else
		echo 'not ok 1 - test sending with three process system is incorrect'
		echo 'Note: assumed without checking that the machine has at least three processors'
		echo "output:$output"
		echo "expected:$expected"
		exit 1
	fi
else
	echo 'not ok 1 - test parsing is correct'
	echo 'Note: assumed without checking that the machine has at least three processors'
	exit 1
fi
