#!/bin/sh
echo 1..1
name="anyName"
output=`mpirun -np 2 src/sendName -n $name`
expected="Hello $name, i am your father!"
if [ $? -eq 0 ]; then
	if [ "$output" == "$expected" ]; then
		echo 'ok 1 - test sending with two process system incorrect'
	else
		echo 'not ok 1 - test sending with two process system incorrect'
		echo "output:$output"
		echo "expected:$expected"
		exit 1
	fi
else
	echo 'not ok 1 - test sending with two process system incorrect'
	exit 1
fi
