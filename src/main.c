#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <argp.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h> //GNU error code like ENOENT 'no such file or directory'. Can be used with parser()

#define SENDER_RANK 0
#define MAX_NAME_LENGTH 100
#define TAG 0
#define PREFIX "Hello my name is,"

#define NAMELESS_MESSAGE 2

#ifndef SYSLOGDIR
#	define SYSLOGDIR "/etc"
#endif
#ifndef BASENAME
#	define BASENAME "sendName.log"
#endif

/*
 * EXIT CODES:
 * 0 Execution with no errors
 * 1 Incorrect command line input arguments
 * 2 NAMELESS_MESSAGE
 */

static char * logfilename = NULL;

typedef enum {false, true} bool ;

bool verbose = false;
char * name = NULL;
static int rank, world_size;

static void openLogFile()
{

	static FILE * logfile = NULL;
	logfilename = (char*)malloc(sizeof(char)*(strlen(SYSLOGDIR)+strlen(BASENAME)+2));
	strcpy(logfilename, SYSLOGDIR);
	strcat(logfilename, "/");
	strcat(logfilename, BASENAME);
	mkdir(SYSLOGDIR, S_IRUSR | S_IWUSR);
	logfile = fopen(logfilename, "w");
	if( logfile != NULL)
		printf("Log file openned succeffully!\n");
	else
		printf("Could not open file!\n");
	fclose(logfile);

}

/* function to send message to another process. 
 * This function also received a message from another process.
 */
static void communicate()
{
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	if( rank == SENDER_RANK)
	{
		char * message = (char*)malloc(sizeof(char)*(strlen(PREFIX)+strlen(name)+1));
		strcpy(message, PREFIX);
		strcat(message, name);
		int destination = 1;
		for(; destination < world_size; destination++ )
		{
			MPI_Send(message, strlen(message)+1, MPI_CHAR, destination, TAG, MPI_COMM_WORLD);
		}
	}
	else
	{
		name = (char *)malloc(sizeof(char)*MAX_NAME_LENGTH);
		int SOURCE = SENDER_RANK;
		MPI_Recv(name, MAX_NAME_LENGTH, MPI_CHAR, SOURCE, TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		char * charPtr = strchr(name, ',');
		if( charPtr != NULL )
		{
			printf("Hello %s, i am your father!\n", charPtr+1);
		}
		else
		{
			printf("Exiting- No name found in message!:%s\n", name);
			MPI_Finalize();
			exit(NAMELESS_MESSAGE);
		}
	}
}

struct arguments
{
	char * name;
	int verbose;
};

/* Function for command line argument parsing */
static error_t parser(int key, char * arg, struct argp_state * state)
{
	struct arguments * defaults = state->input;
	switch(key)
	{
		case 'v':
			verbose = true;
			printf("Verbose option set.\n");
			break;
		case 'n':
			name = arg;
			//Nothing to do since there is no name argument yet
			break; 
		case ARGP_KEY_ARG://Sometimes true for non-optional arguments
			name = arg;
			if( verbose )
				printf("Name:%s\n", arg);
			return ARGP_ERR_UNKNOWN;
			break;
		case ARGP_KEY_END:
			if ( name == NULL )
			{
				printf("Exiting, because no name message was provided.\n");
				argp_usage(state);
			}
			break;
		default:
			{
				return ARGP_ERR_UNKNOWN;
			}
	}
	return 0;
}

/* main function- start execution here by specifying commandline args, read command line and call communicate() */
int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	

	if( rank == SENDER_RANK)
	{
		const char * argp_program_version = "1.0";
		const char * argp_program_bug_address = "1princesibanda@gmail.com";

		static struct arguments defaultValues;
		defaultValues.verbose = false;
		defaultValues.name = "noName";

		static char usage_doc[] = "-n name";
		static char program_description[] = "This is a distributed program that sends a name message between (at least two of) its processes.";
		static const struct argp_option options[] =	{
								{ "verbose", 'v', NULL, OPTION_ARG_OPTIONAL, "Set verbose on.", 0 },
								{ "name", 'n', "NameToMessage", 0, "The name to message between processes.", 0 },
								{ 0 }
								};
		static const struct argp argp = { options, parser, usage_doc, program_description };

		argp_parse(&argp, argc, argv, ARGP_IN_ORDER, NULL, &defaultValues);
	}

	communicate();
	MPI_Finalize();
	return 0;
}
